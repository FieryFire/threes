cc:
	g++ main.cpp -std=c++11 -O3 -o main -pthread Game/player_evil_game.cpp Game/threes.cpp AI/PEbot.cpp AI/Random/randomBot.cpp AI/BottomLeft/ldBot.cpp AI/TD0/td0Bot.cpp AI/TD0/VTable.cpp
cc4:
	g++ main.cpp -std=c++11 -O3 -o main -pthread Game/player_evil_game.cpp Game/threes.cpp AI/PEbot.cpp AI/TD0/td0Botp4.cpp AI/TD0/VTable.cpp
cc4m:
	g++ main.cpp -std=c++11 -O3 -o main -pthread Game/player_evil_game.cpp Game/threes.cpp AI/PEbot.cpp AI/TD0_multiple/td0Botp4.cpp AI/TD0_multiple/VTable.cpp
clean:
	rm globalHeaders.h Game/threes.cpp -f
clean_all:
	rm globalHeaders.h Game/threes.cpp weights/weight* -f
p1_c:
	cp headers/p1Headers.h .
	mv p1Headers.h globalHeaders.h
p2_c:
	cp headers/p2Headers.h .
	mv p2Headers.h globalHeaders.h
	cp weights/6x15/* weights
p4_c:
	cp headers/p4Headers.h .
	mv p4Headers.h globalHeaders.h
p4m_c:
	cp headers/p4Headers_m.h .
	mv p4Headers_m.h globalHeaders.h
threes_simple:
	cp Game/gameModules/threes_simple.cpp Game
	mv Game/threes_simple.cpp Game/threes.cpp
threes_hard:
	cp Game/gameModules/threes_hard.cpp Game
	mv Game/threes_hard.cpp Game/threes.cpp

p1: clean_all p1_c threes_simple cc
p2: clean_all p2_c threes_simple cc
p4: clean p4_c threes_hard cc4
p4_test: clean p4_c threes_simple cc4
p4m: clean p4m_c threes_hard cc4m
p4m_test: clean p4m_c threes_simple cc4m