##Threes!
**A project for NCTU Theory of Computer Gaming lecture.**

####About

This project contains the console version of Threes! game, and some bots which can play the game.

The Game folder contains the Threes! game, and the game prototype for flexible developing.

The AI folder contains the bots.  There's also different kinds of algorithm and strategy, seperated in different subfolders.

####Details

Language: C++11

Environment: Windows/Linux

####Usage

(TBD)

Command Line Options:

	--total=[t]       let the computer play *t* times
	
	--path=[file]     dump the gameplay into the specific file
	
	--debug           view the gameplay of the bot on the screen,the program will pause on each step.