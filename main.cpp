#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <chrono>
#include <cmath>

#include "globalHeaders.h"

using namespace std;
using namespace std::chrono;

int debug=0;
int train=0;
int csv=0;
int rounds=0;
int hasevil=0;
fstream dump;
char filepath[30]="";

string buffer="";
milliseconds gameclock;

int prefix(const char *,const char *);
int getinput(int,char **);
void statistics();
void manuel(PEGame &,PEBot &);
void autorun(PEGame &,PEBot &);
void training(PEGame &,PEBot &);

int main(int argc,char **argv){
	int flag=getinput(argc,argv);
	time_t t=time(NULL);
	if(flag){
		return 1;
	}
	Threes game;
	UsedBot bot;
	if(!train&&filepath[0]){
		game=Threes(9,hasevil,0,&buffer,&gameclock);
		bot=UsedBot(game,&buffer,&gameclock);
	}else{
		game=Threes(9,hasevil,0);
		bot=UsedBot(game);
	}
	if(debug){
		manuel(game,bot);
	}else if(train){
		training(game,bot);
	}else{
		autorun(game,bot);
	}
	if(filepath[0]){
		dump.close();
	}
	if(!debug)
		cout<<"Time elapsed: "<<time(NULL)-t<<" seconds.\n";
	return 0;
}

int prefix(const char *a, const char *b){
	int i=0;
	while(a[i]&&b[i]){
		if(a[i]!=b[i]) return 0;
		i++;
	}
	return 1;
}

int getinput(int argc,char **argv){
	int it=1;
	for(;it<argc;it++){
		if(!strcmp(argv[it],"--debug")){
			debug=1;
		}else if(!strcmp(argv[it],"--train")){
			train=1;
		}else if(!strcmp(argv[it],"--csv")){
			csv=1;
		}else if(prefix(argv[it],"--save=")){
			try{
				dump.open(argv[it]+7,fstream::out);
			}catch(exception e){
				cerr<<"Invalid output path.\n";
				return 1;
			}
			strcpy(filepath,argv[it]+7);
		}else if(prefix(argv[it],"--total=")){
			try{
				rounds=atoi(argv[it]+8);
			}catch(exception e){
				cerr<<"Invalid round number.\n";
			}
		}else{
			cerr<<"Unknown command: "<<argv[it]<<'\n';
			return 1;
		}
	}
	return 0;
}

void statistics(){
	;
}

void manuel(PEGame &game, PEBot &bot){//Windows only
	int flag=0;
	if(filepath[0]){
		gameclock=duration_cast<milliseconds>(system_clock::now().time_since_epoch());
		dump<<bot.getName()<<":random@"<<gameclock.count()<<'|';
	}
	game.init();
	while(1){
		system("cls");
		cout<<"Board:\n";
		game.draw();
		cout<<'\n';
		cout<<"Current score: "<<game.getScore()<<'\n';
		if(flag){
			cout<<"Game Over!\n";
			break;
		}
		system("pause");
		bot.play(game);
		if(!filepath[0]&&buffer.length()>1000){
			dump<<buffer;
			buffer="";
		}
		if(game.checkDead()) flag=1;
	}
	if(filepath[0]){
		gameclock=duration_cast<milliseconds>(system_clock::now().time_since_epoch());
		dump<<buffer<<"|random@"<<gameclock.count()<<'\n';
		buffer="";
	}
}

void autorun(PEGame &game, PEBot &bot){
	int flag;
	for(int imp=0;imp<rounds;imp++){
		flag=0;
		if(filepath[0]){
			gameclock=duration_cast<milliseconds>(system_clock::now().time_since_epoch());
			dump<<bot.getName()<<":random@"<<gameclock.count()<<'|';
		}
		game.init();
		while(1){
			if(flag)
				break;
			bot.play(game);
			if(!filepath[0]&&buffer.length()>1000){
				dump<<buffer;
				buffer="";
			}
			if(game.checkDead()) flag=1;
		}
		if(filepath[0]){
			gameclock=duration_cast<milliseconds>(system_clock::now().time_since_epoch());
			dump<<buffer<<"|random@"<<gameclock.count()<<'\n';
			buffer="";
		}
	}
}

void training(PEGame &game,PEBot &bot){
	int flag;
	if(!rounds){
		rounds=0x7fffffff;
	}
	fstream scoreRecord;
	if(csv){
		scoreRecord.open("score.csv",fstream::out);
		scoreRecord<<"sep=;\n";
	}
	try{
		for(int imp=0;imp<rounds;){
			flag=0;
			game.init();
			while(1){
				if(flag)
					break;
				bot.play(game);
				if(game.checkDead()) flag=1;
			}
			imp++;
			if(csv){
				if(imp%20==0){
					scoreRecord<<log(game.getScore())/log(3)<<";=average(A"<<imp/20<<":T"<<imp/20+9<<")\n";
				}else{
					scoreRecord<<log(game.getScore())/log(3)<<";";
				}
			}
		}
	}catch(exception e){
		;
	}
	try{
		scoreRecord.close();
	}catch(exception e){
		;
	}
}
