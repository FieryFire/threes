#include "threes.h"
#include "player_evil_game.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>
#include <chrono>

Threes::Threes(){
	;
}

Threes::Threes(int st,int e,int et){
	PEGame::stateN=16;
	PEGame::actionN=4;
	
	int a[12]={1,1,1,1,2,2,2,2,3,3,3,3};
	tileBack=std::vector<int>(a,a+12);
	tileBag=std::vector<int>(tileBack);
	
	for(int i=0;i<16;i++){
		board[i]=0;
	}
	
	srand(time(NULL));
	
	startTile=st;
	evil=e;
	evilTile=et;
	buff=NULL;
	gameclock=NULL;
}

Threes::Threes(int st,int e,int et,std::string *b,std::chrono::milliseconds *ms){
	PEGame::stateN=16;
	PEGame::actionN=4;
	
	int a[12]={1,1,1,1,2,2,2,2,3,3,3,3};
	tileBack=std::vector<int>(a,a+12);
	tileBag=std::vector<int>(tileBack);
	
	for(int i=0;i<16;i++){
		board[i]=0;
	}
	
	srand(time(NULL));
	
	startTile=st;
	evil=e;
	evilTile=et;
	buff=b;
	gameclock=ms;
}

int Threes::b2i(int block){
	int a=0;
	switch(block){
		case 1:
			a=1;
			break;
		case 2:
			a=2;
			break;
		case 3:
			a=3;
			break;
		case 4:
			a=6;
			break;
		case 5:
			a=12;
			break;
		case 6:
			a=24;
			break;
		case 7:
			a=48;
			break;
		case 8:
			a=96;
			break;
		case 9:
			a=192;
			break;
		case 10:
			a=384;
			break;
		case 11:
			a=768;
			break;
		case 12:
			a=1536;
			break;
		case 13:
			a=3072;
			break;
		case 14:
			a=6144;
			break;
	}
	return a;
}

std::string Threes::b2s(int block){
	std::string a="    ";
	switch(block){
		case 1:
			a="  1 ";
			break;
		case 2:
			a="  2 ";
			break;
		case 3:
			a="  3 ";
			break;
		case 4:
			a="  6 ";
			break;
		case 5:
			a=" 12 ";
			break;
		case 6:
			a=" 24 ";
			break;
		case 7:
			a=" 48 ";
			break;
		case 8:
			a=" 96 ";
			break;
		case 9:
			a=" 192";
			break;
		case 10:
			a=" 384";
			break;
		case 11:
			a=" 768";
			break;
		case 12:
			a="1536";
			break;
		case 13:
			a="3072";
			break;
		case 14:
			a="6144";
			break;
	}
	return a;
}

int Threes::bScore(int block){
	int a=1;
	if(block<3) return 0;
	for(;block>2;block--){
		a*=3;
	}
	return a;
}

int bmax(int n,int *b){
	int maxtile=0;
	for(int i=0;i<n;i++){
		maxtile=b[i]>maxtile?b[i]:maxtile;
	}
	return maxtile;
}

void Threes::pushTile(){
	int maxt=bmax(16,board);
	if(maxt>6&&(float)(steps+1)/(bonus+1)>=21.0&&rand()%21==0){
		int choice=maxt-6;
		nextTile=rand()%choice+4;
	}else{
		int tmp=rand()%tileBag.size();
		nextTile=tileBag[tmp];
		tileBag.erase(tileBag.begin()+tmp);
		
		if(tileBag.size()==0){
			tileBag=std::vector<int>(tileBack);
		}
	}
	++steps;
}

void Threes::genTile(){
	std::vector<int> pos;
	for(int i=0;i<16;i++){
		if(board[i]==0) pos.push_back(i);
	}
	int ptmp=rand()%pos.size();
	board[pos[ptmp]]=nextTile;
	PEGame::score+=bScore(nextTile);
	if(buff){
		std::chrono::milliseconds tmp=std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
		std::stringstream ss;
		ss<<std::hex<<std::uppercase<<pos[ptmp]<<std::dec<<nextTile;
		if(tmp>(*gameclock)){
			ss<<'('<<(tmp-(*gameclock)).count()<<')';
			(*gameclock)=tmp;
		}
		std::string s(ss.str());
		(*buff).append(s);
	}
	pushTile();
	setDead();
}

void Threes::genTile(int dir,int record){
	if(PEGame::phase-1||evil) return;
	std::vector<int> pos;
	switch(dir){
		case 0:
			if(board[12]==0) pos.push_back(12);
			if(board[13]==0) pos.push_back(13);
			if(board[14]==0) pos.push_back(14);
			if(board[15]==0) pos.push_back(15);
			break;
		case 1:
			if(board[0]==0) pos.push_back(0);
			if(board[1]==0) pos.push_back(1);
			if(board[2]==0) pos.push_back(2);
			if(board[3]==0) pos.push_back(3);
			break;
		case 2:
			if(board[3]==0) pos.push_back(3);
			if(board[7]==0) pos.push_back(7);
			if(board[11]==0) pos.push_back(11);
			if(board[15]==0) pos.push_back(15);
			break;
		case 3:
			if(board[0]==0) pos.push_back(0);
			if(board[4]==0) pos.push_back(4);
			if(board[8]==0) pos.push_back(8);
			if(board[12]==0) pos.push_back(12);
			break;
	}
	int ptmp=rand()%pos.size();
	board[pos[ptmp]]=nextTile;
	PEGame::score+=bScore(nextTile);
	if(record&&buff){
		std::chrono::milliseconds tmp=std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
		std::stringstream ss;
		ss<<std::hex<<std::uppercase<<pos[ptmp]<<std::dec<<nextTile;
		if(tmp>(*gameclock)){
			ss<<'('<<(tmp-(*gameclock)).count()<<')';
			(*gameclock)=tmp;
		}
		std::string s(ss.str());
		(*buff).append(s);
	}
	pushTile();
	setDead();
}

void Threes::setDead(){
	int flag=1;
	for(int i=0;i<16;i++){
		if(board[i]==0){
			flag=0;
			break;
		}
		if(i%4<3){
			if(board[i+1]+board[i]==3){
				flag=0;
				break;
			}else if(board[i+1]==board[i]&&board[i]>2){
				flag=0;
				break;
			}
		}
		if(i<12){
			if(board[i+4]+board[i]==3){
				flag=0;
				break;
			}else if(board[i+4]==board[i]&&board[i]>2){
				flag=0;
				break;
			}
		}
	}
	if(flag){
		PEGame::phase=2;
	}else{
		PEGame::phase=0;
	}
}

void Threes::makeBoard(){
	int nowScore=0;
	for(int i=0;i<16;i++){
		nowScore+=bScore(board[i]);
	}
	PEGame::score=nowScore;
	PEGame::phase=0;
	setDead();
}

int Threes::play(int action){
	if(PEGame::phase) return INVALID;
	int _act=1;
	int tmpscore=0;
	switch(action){
		case 0://up
			for(int i=1;i<4;i++){
				for(int j=0;j<4;j++){
					if(board[i*4+j]==0) continue;
					if(board[(i-1)*4+j]==0){
						board[(i-1)*4+j]=board[i*4+j];
						board[i*4+j]=0;
						_act=0;
					}else if(board[(i-1)*4+j]+board[i*4+j]==3){
						tmpscore+=3;
						board[(i-1)*4+j]=3;
						board[i*4+j]=0;
						_act=0;
					}else if(board[(i-1)*4+j]==board[i*4+j]&&board[i*4+j]>2){
						tmpscore+=bScore(board[i*4+j]);
						board[(i-1)*4+j]+=1;
						board[i*4+j]=0;
						_act=0;
					}
				}
			}
			break;
		case 1://down
			for(int i=2;i>=0;i--){
				for(int j=0;j<4;j++){
					if(board[i*4+j]==0) continue;
					if(board[(i+1)*4+j]==0){
						board[(i+1)*4+j]=board[i*4+j];
						board[i*4+j]=0;
						_act=0;
					}else if(board[(i+1)*4+j]+board[i*4+j]==3){
						tmpscore+=3;
						board[(i+1)*4+j]=3;
						board[i*4+j]=0;
						_act=0;
					}else if(board[(i+1)*4+j]==board[i*4+j]&&board[i*4+j]>2){
						tmpscore+=bScore(board[i*4+j]);
						board[(i+1)*4+j]+=1;
						board[i*4+j]=0;
						_act=0;
					}
				}
			}
			break;
		case 2://left
			for(int j=1;j<4;j++){
				for(int i=0;i<4;i++){
					if(board[i*4+j]==0) continue;
					if(board[i*4+j-1]==0){
						board[i*4+j-1]=board[i*4+j];
						board[i*4+j]=0;
						_act=0;
					}else if(board[i*4+j-1]+board[i*4+j]==3){
						tmpscore+=3;
						board[i*4+j-1]=3;
						board[i*4+j]=0;
						_act=0;
					}else if(board[i*4+j-1]==board[i*4+j]&&board[i*4+j]>2){
						tmpscore+=bScore(board[i*4+j]);
						board[i*4+j-1]+=1;
						board[i*4+j]=0;
						_act=0;
					}
				}
			}
			break;
		case 3://right
			for(int j=2;j>=0;j--){
				for(int i=0;i<4;i++){
					if(board[i*4+j]==0) continue;
					if(board[i*4+j+1]==0){
						board[i*4+j+1]=board[i*4+j];
						board[i*4+j]=0;
						_act=0;
					}else if(board[i*4+j+1]+board[i*4+j]==3){
						tmpscore+=3;
						board[i*4+j+1]=3;
						board[i*4+j]=0;
						_act=0;
					}else if(board[i*4+j+1]==board[i*4+j]&&board[i*4+j]>2){
						tmpscore+=bScore(board[i*4+j]);
						board[i*4+j+1]+=1;
						board[i*4+j]=0;
						_act=0;
					}
				}
			}
			break;
	}
	if(_act) return NOMOVE;
	PEGame::score+=tmpscore;
	PEGame::phase=1;
	return tmpscore;
}

int Threes::place(int p){
	if(PEGame::phase-1||!evil||evilTile) return INVALID;
	if(board[p]) return INVALID;
	board[p]=nextTile;
	PEGame::score+=bScore(nextTile);
	pushTile();
	setDead();
	PEGame::phase=0;
	return 0;
}

int Threes::place(int p,int b){
	if(PEGame::phase-1||!evil||!evilTile) return INVALID;
	if(board[p]) return INVALID;
	std::vector<int>::iterator it=tileBag.begin();
	for(;it!=tileBag.end();it++){
		if(*it==b) break;
	}
	if(it==tileBag.end()) return INVALID;
	board[p]=nextTile;
	tileBag.erase(it);
	if(tileBag.size()==0)
		tileBag=std::vector<int>(tileBack);
	PEGame::score+=bScore(b);
	PEGame::phase=0;
	return 0;
}
	
void Threes::init(){
	tileBag=std::vector<int>(tileBack);
	steps=bonus=0;
	for(int i=0;i<16;i++){
		board[i]=0;
	}
	pushTile();
	for(int i=0;i<startTile;i++){
		genTile();
	}
	makeBoard();
	PEGame::phase=0;
}

void Threes::set(int p,int b){
	board[p]=b;
	makeBoard();
}

void Threes::set(std::vector<int> m){
	int tmp;
	if(m.size()!=16) return;
	for(tmp=0;tmp<16;tmp++){
		if(m[tmp]<0||m[tmp]>15) break;
	}
	if(tmp!=16) return;
	for(tmp=0;tmp<16;tmp++){
		board[tmp]=m[tmp];
	}
	makeBoard();
}

void Threes::store(){
	stateStore.tileBag=tileBag;
	for(int i=0;i<16;i++){
		stateStore.board[i]=board[i];
	}
	stateStore.nextTile=nextTile;
	stateStore.steps=steps;
	stateStore.bonus=bonus;
}

void Threes::restore(){
	tileBag=stateStore.tileBag;
	for(int i=0;i<16;i++){
		board[i]=stateStore.board[i];
	}
	nextTile=stateStore.nextTile;
	steps=stateStore.steps;
	bonus=stateStore.bonus;
	makeBoard();
}

void swap(int &a, int &b){
	int tmp=a;
	a=b;
	b=tmp;
}

void Threes::rotate(){
	swap(board[0],board[3]);
	swap(board[0],board[15]);
	swap(board[0],board[12]);
	swap(board[1],board[7]);
	swap(board[1],board[14]);
	swap(board[1],board[8]);
	swap(board[2],board[11]);
	swap(board[2],board[13]);
	swap(board[2],board[4]);
	swap(board[5],board[6]);
	swap(board[5],board[10]);
	swap(board[5],board[9]);
}

void Threes::flip(){
	swap(board[0],board[3]);
	swap(board[1],board[2]);
	swap(board[4],board[7]);
	swap(board[5],board[6]);
	swap(board[8],board[11]);
	swap(board[9],board[10]);
	swap(board[12],board[15]);
	swap(board[13],board[14]);
}

std::vector<int> Threes::getState(){
	return std::vector<int>(board,board+16);
}

int Threes::getNext(){
	return nextTile>3?4:nextTile;
}

std::vector<int> Threes::getPlace(){
	std::vector<int> pos=std::vector<int>();
	for(int i=0;i<16;i++){
		if(!board[i])
			pos.push_back(i);
	}
	return pos;
}

std::vector<int> Threes::getTiles(){
	return tileBag;
}

void Threes::draw(){
	//system("cls");
	std::cout<<"+----+----+----+----+\n";
	std::cout<<"|"<<b2s(board[0])<<"|"<<b2s(board[1])<<"|"<<b2s(board[2])<<"|"<<b2s(board[3])<<"|\n";
	std::cout<<"+----+----+----+----+\n";
	std::cout<<"|"<<b2s(board[4])<<"|"<<b2s(board[5])<<"|"<<b2s(board[6])<<"|"<<b2s(board[7])<<"|\n";
	std::cout<<"+----+----+----+----+\n";
	std::cout<<"|"<<b2s(board[8])<<"|"<<b2s(board[9])<<"|"<<b2s(board[10])<<"|"<<b2s(board[11])<<"|\n";
	std::cout<<"+----+----+----+----+\n";
	std::cout<<"|"<<b2s(board[12])<<"|"<<b2s(board[13])<<"|"<<b2s(board[14])<<"|"<<b2s(board[15])<<"|\n";
	std::cout<<"+----+----+----+----+\n";
}