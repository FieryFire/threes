#ifndef _PLAYER_EVIL_GAME_H_
#define _PLAYER_EVIL_GAME_H_

#define NOMOVE -9999999
#define INVALID -10000000
#include <vector>

class PEGame{
	protected:
		int stateN; //# of numbers in a state, or the features
		int actionN; //# of actions
		int score; //Total score in current gameplay
		int phase; //0: player phase; 1: evil phase; 2:game over
		
	public:
		PEGame();
		
		//Playing-related methods
		//Player action
		virtual int play(int);
		//Evil action
		virtual int place(int);
		virtual int place(int,int);
		//Initialize/restart
		virtual void init();
		//Set board
		virtual void set(int,int);
		virtual void set(std::vector<int>);
		//Store/restore state
		virtual void store();
		virtual void restore();
		//Generate tile
		virtual void genTile();
		virtual void genTile(int,int record=1);
		//Rotate/Flip the board
		virtual void rotate();
		virtual void flip();
		
		//Game-reference-related methods
		//Get # of the features
		int getStateN();
		//Get the current game state
		virtual std::vector<int> getState();
		//Get the # of (possible) actions (for player)
		int getActionN();
		//Get the next tile (for both), may not be implemented
		virtual int getNext();
		//Get the spots can be put with tiles (for evil)
		virtual std::vector<int> getPlace();
		//Get the tiles which can be played (for evil)
		virtual std::vector<int> getTiles();
		//Check if the game is over
		int checkDead();
		
		//Statastic/Debug-related methods
		virtual void draw(); //Print the board to the screen
		int getScore(); //Get the current score
		int getPhase(); //Get the current phase
};

#endif