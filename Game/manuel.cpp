#include <iostream>
#include <conio.h>
#include <cstdlib>
#include "player_evil_game.h"
#include "threes.h"

using namespace std;

int main(){
	Threes game=Threes(9,0,0);
	char c;
	int a,r=0,flag=0;
	game.init();
	while(1){
		system("cls");
		cout<<"Board:\n";
		game.draw();
		cout<<'\n';
		cout<<"Current score: "<<game.getScore()<<'\n';
		
		cout<<"Next tile: "<<game.getNext()<<'\n';
		/*
		cout<<"Phase: "<<game.getPhase()<<'\n';
		
		cout<<"State: ";
		vector<int> v=game.getState();
		for(int i=0;i<16;i++)
			cout<<v[i]<<' ';
		cout<<'\n';
		
		cout<<"Tiles: ";
		v=game.getTiles();
		for(int i=0;i<v.size();i++){
			cout<<v[i]<<' ';
		}
		cout<<'\n';
		
		cout<<"r: "<<r<<'\n';
		*/
		if(flag){
			cout<<"Game Over!\n";
			break;
		}
		
		do{
			c=getch();
			switch(c){
				case 'w':
					a=0;
					break;
				case 's':
					a=1;
					break;
				case 'a':
					a=2;
					break;
				case 'd':
					a=3;
					break;
				case 'q':
					a=4;
					break;
				default:
					a=-1;
					break;
			}
		}while(a<0);
		if(a==4) break;
		r=game.play(a);
		if(game.checkDead()) flag=1;
	}
	return 0;
}