#ifndef _THREES_H_
#define _THREES_H_

#include "player_evil_game.h"
#include <vector>
#include <string>
#include <fstream>
#include <chrono>

class Threes: public PEGame{
	protected:
		std::vector<int> tileBag,tileBack; //tilebags
		int board[16]; //the board of the game
		int nextTile; //prefetch the next Tile for use
		int steps, bonus; //for generating bonus tile
		
		struct{
			std::vector<int> tileBag;
			int board[16];
			int nextTile;
			int steps;
			int bonus;
		}stateStore; //Used for state storing
		
		int startTile; //# of starting tiles in the game
		int evil; //set to true if there's a evil player
		int evilTile; //set to true if evil can choose his tile
		std::string *buff; //used for dump things
		std::chrono::milliseconds *gameclock;
		
		int b2i(int); //decode the block to actual number in game
		std::string b2s(int); //for screen output
		int bScore(int);
		
		void pushTile(); //push a tile from tileBag to nextTile
		void genTile(); //randomly generate tile for initialize
		void setDead();
		void makeBoard();
		
	public:
		Threes();
		Threes(int,int,int);
		Threes(int,int,int,std::string *,std::chrono::milliseconds *);
		
		//Playing-related methods
		int play(int);
		int place(int);
		int place(int, int);
		void init();
		void set(int,int);
		void set(std::vector<int>);
		void store();
		void restore();
		void genTile(int,int record=1); //generate a tile
		void rotate();
		void flip();
		
		//Game-reference-related methods
		std::vector<int> getState();
		int getNext();
		std::vector<int> getPlace();
		std::vector<int> getTiles();
		
		//Statastic/Debug-related methods
		void draw();
};

#endif