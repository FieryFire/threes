#include "player_evil_game.h"

PEGame::PEGame(){
	stateN=0;
	actionN=1;
	score=0;
	phase=0;
}

int PEGame::play(int action){
	if(phase) return INVALID;
	if(action-actionN<0) return INVALID;
	phase=1;
	return 0;
}

int PEGame::place(int spot){
	if((phase-1)) return INVALID;
	phase=0;
	return 0;
}

int PEGame::place(int spot,int block){
	if((phase-1)) return INVALID;
	phase=0;
	return 0;
}

void PEGame::init(){
	phase=0;
}

void PEGame::set(int p,int i){
	;
}

void PEGame::set(std::vector<int> m){
	;
}

void PEGame::store(){
	;
}

void PEGame::restore(){
	;
}

void PEGame::genTile(){
	;
}

void PEGame::genTile(int dir,int record){
	;
}

void PEGame::rotate(){
	;
}

void PEGame::flip(){
	;
}

int PEGame::getStateN(){
	return stateN;
}

std::vector<int> PEGame::getState(){
	return std::vector<int>();
}

int PEGame::getActionN(){
	return actionN;
}

int PEGame::getNext(){
	return 0;
}

std::vector<int> PEGame::getPlace(){
	return std::vector<int>();
}

std::vector<int> PEGame::getTiles(){
	return std::vector<int>();
}

int PEGame::checkDead(){
	return !(phase-2);
}

void PEGame::draw(){
	;
}

int PEGame::getScore(){
	return score;
}

int PEGame::getPhase(){
	return phase;
}