#include "PEbot.h"
#include <string>
#include <chrono>
#include <sstream>

PEBot::PEBot(){
	actions=1;
	features=0;
	dump=NULL;
	gameclock=NULL;
}

PEBot::PEBot(PEGame &game){
	actions=game.getActionN();
	features=game.getStateN();
	dump=NULL;
	gameclock=NULL;
}

PEBot::PEBot(PEGame &game,std::string *buff,std::chrono::milliseconds *ms){
	actions=game.getActionN();
	features=game.getStateN();
	dump=buff;
	gameclock=ms;
}

void PEBot::record(int act,int reward){
	if(!dump) return;
	std::chrono::milliseconds ms=std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
	std::stringstream ss;
	std::string s;
	switch(act){
		case 0:
			s="#U";
			break;
		case 1:
			s="#D";
			break;
		case 2:
			s="#L";
			break;
		case 3:
			s="#R";
			break;
	}
	ss<<s;
	if(reward>0){
		ss<<'['<<reward<<']';
	}
	if(ms>(*gameclock)){
		ss<<'('<<(ms-(*gameclock)).count()<<')';
		*gameclock=ms;
	}
	(*dump).append(ss.str());
}

void PEBot::play(PEGame &game){
	game.play(0);
}

std::string PEBot::getName(){
	return name;
}