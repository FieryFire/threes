#include "td0Bot.h"

#include <cstdlib>
#include <ctime>
#include <cmath>
#include <string>
#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <cstring>
#include <chrono>
//#include <pthread.h>

int amax(const int n,const prc *v){
	prc m=-9999999;
	for(int i=0;i<n;i++){
		m=m<v[i]?v[i]:m;
	}
	for(int i=0;i<n;i++){
		if(v[i]==m) return i;
	}
}

int vmax(std::vector<int> v){
	if(!v.size()) return 0;
	int tmp=v[0];
	for(int i=0;i<v.size();i++){
		tmp=tmp<v[i]?v[i]:tmp;
	}
	return tmp;
}

TD0Bot::TD0Bot():PEBot(){
	PEBot::name="TD0";
}

TD0Bot::TD0Bot(PEGame &game):PEBot(game){
	PEBot::name="TD0";
	init();
}

TD0Bot::TD0Bot(PEGame &game,std::string *buff,std::chrono::milliseconds *ms):PEBot(game,buff,ms){
	PEBot::name="TD0";
	init();
}

void TD0Bot::destruct(){
	eval_t.destruct();
	target_t.destruct();
}

void TD0Bot::init(){
	//v2 6*4*4
	int tlength=15;
	int tsize[15]={6,6,6,6,6,6,6,6,6,6,6,6,6,6,6};
	int tblocks[15][6]={{0,1,2,3,4,5},{4,5,6,7,8,9},{5,6,7,9,10,11},{9,10,11,13,14,15},{0,1,2,3,6,7},{4,5,6,7,10,11},{4,5,6,8,9,10},{8,9,10,12,13,14},{0,1,4,5,8,12},{1,2,5,6,9,13},{5,6,9,10,13,14},{6,7,10,11,14,15},{2,3,6,7,11,15},{1,2,5,6,10,14},{4,5,8,9,12,13}};

	for(int i=0;i<tlength;i++){
		tuples.push_back(std::vector<int>(tblocks[i],tblocks[i]+tsize[i]));
	}
	//changable variables:
	//v2
	trained=5000000;
	round=5000000;
	search_max=0;
	train_max=0;
	updateCycle=5;
	storeCycle=500000;
	record_max=10;
	
	
	fast_up=0.95;
	search_path=0.95;
	/*
	fast_up=1.0;
	search_path=0.95;
	*/
	gamma=0.99;
	eval_t=VTable(tuples);
	if(trained){
		eval_t.restore(trained);
	}else{
		eval_t.init();
	}
	if(round<train_max){
		target_t=VTable(tuples);
		target_t.copy(eval_t);
	}
	
	cstep=0;
	step_max=0;
	srand(time(NULL));
}

int TD0Bot::maxTile(std::vector<int> s){
	int i=0;
	for(int imp=0;imp<16;imp++){
		i=i<s[imp]?s[imp]:i;
	}
	return i;
}

prc TD0Bot::reward(std::vector<int> S0,std::vector<int> S1){
	int m=maxTile(S0)+1;
	int bc0[16],bc1[16];
	for(int i=0;i<16;i++){
		bc0[S0[i]]++;
		bc1[S1[i]]++;
	}
	prc r=(prc)0;
	for(int i=m;i>=4;i--){
		S1[i]-=S0[i];
		if(S1[i]){
			S0[i-1]-=S1[i]*2;
			r+=pow(1.02,pow(2,i-4));
		}
	}
	return r;
}

prc TD0Bot::penalty(int step){
	//return -5*pow(1.015,(double)step/(double)3);
	return -20*log2(step/3);
}

void TD0Bot::play(PEGame &game){
	int reward,act,boardmax;
	std::vector<int> s0,a0,s1;
	s0=game.getState();
	
	boardmax=vmax(s0);
	if(round>=train_max)
		e_greedy=2.0;
	else if(cstep<step_max-30)
		e_greedy=fast_up;
	else
		e_greedy=search_path;
	//action strategy
	if((double)rand()/(double)RAND_MAX<e_greedy){
		//peek the next state and find the
		game.store();
		prc asv[4];
		for(int i=0;i<4;i++){
			reward=game.play(i);
			if(reward==NOMOVE){
				asv[i]=reward;
				continue;
			}
			game.genTile(i,0);
			s1=game.getState();
			asv[i]=eval_t.getValue(s1);
			game.restore();
		}
		act=amax(4,asv);
		reward=game.play(act);
	}else{
		//randomly action for a small probability to search path
		do{
			act=rand()%4;
			reward=game.play(act);
		}while(reward==NOMOVE);
	}
	a0=game.getState();
	
	if(reward!=INVALID){
		PEBot::record(act,reward);
	}
	
	game.genTile(act);
	
	cstep++;
	
	if(round<train_max){
		s1=game.getState();
		if(DTABLE){
			prc r2=TD0Bot::reward(s0,a0)+gamma*target_t.getValue(s1);
			target_t.update(s0,r2,round);
			if(game.checkDead()){
				target_t.update(s1,penalty(cstep),round);
			}
		}else{
			prc r2=TD0Bot::reward(s0,a0)+gamma*eval_t.getValue(s1);
			eval_t.update(s0,r2,round);
			if(game.checkDead()){
				eval_t.update(s1,penalty(cstep),round);
			}
		}
	}
	if(game.checkDead()){
		step_max=cstep>step_max?cstep:step_max;
		cstep=0;
		boardmax=vmax(game.getState());
		maxtile=maxtile<boardmax?boardmax:maxtile;
		round++;
		if(round<=train_max&&round%updateCycle==0){
			eval_t.copy(target_t);
		}
		if(fast_up<1)
			;//fast_up=search_path=fast_up+0.1/search_max;
		/*
		if(round==search_max){
			fast_up=1.0;
			search_path=0.9;
		}
		*/
		if(round<=train_max&&round%storeCycle==0){
			target_t.store(round);
			if(wrecord.size()==record_max){
				eval_t.del(wrecord[0]);
				wrecord.erase(wrecord.begin());
			}
			wrecord.push_back(round);
		}
		if(round<=train_max&&round%10000==0){
			std::cout<<"Round: "<<round<<'\n';
		}
	}
}
