#include "td0Bot.h"

//#include <cstdlib>
//#include <ctime>
#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
//#include <chrono>
#include <pthread.h>

int power16(const int p){
	return 1<<(4*p);
}

VTable::VTable(){
	;
}

VTable::VTable(std::vector<std::vector<int> > &t){
	for(int i=0;i<t.size();i++){
		tablesize.push_back(power16(t[i].size()));
	}
	tuples=t;
	values=new prc *[tablesize.size()];
	for(int i=0;i<tablesize.size();i++){
		values[i]=new prc[tablesize[i]];
	}
	
	if(CPTH){
		packs=new package[tablesize.size()];
		threads=new pthread_t[tablesize.size()];
	}
	
	//alpha=(prc)0.1/(prc)t.size();
	int s=0;
	for(int i=0;i<t.size();i++){
		s+=t[i].size();
	}
	alpha=(prc)0.1/(prc)s;
}

void VTable::destruct(){
	for(int i=0;i<tablesize.size();i++){
		delete values[i];
	}
	delete values;
}

std::vector<int> VTable::hashTuple(std::vector<int> s){
	std::vector<int> t;
	for(int i=0;i<tuples.size();i++){
		int tmp=0;
		for(int j=0;j<tuples[i].size();j++){
			tmp+=s[tuples[i][j]]*power16(j);
		}
		t.push_back(tmp);
	}
	return t;
}

prc VTable::getValue(std::vector<int> s){
	std::vector<int> h=hashTuple(s);
	prc v=0;
	for(int i=0;i<tuples.size();i++){
		v+=values[i][h[i]];
	}
	return v;
}

prc alphaDecay(int r){
	return pow(2,-(double)r/500000);
}

void VTable::update(std::vector<int> s,prc v,int r){
	std::vector<int> h=hashTuple(s);
	prc e=getValue(s);
	for(int i=0;i<h.size();i++){
		values[i][h[i]]+=alpha*alphaDecay(r)*(v-getValue(s));
	}
}

void *copythread(void *args){
	package pack=*(package *)args;
	memcpy(pack.vd->values[pack.n],pack.vs->values[pack.n],pack.vd->tablesize[pack.n]*sizeof(prc));
	pthread_exit(0);
}

void VTable::copy(VTable &vt){
	if(CPTH){
		for(int i=0;i<tablesize.size();i++){
			packs[i].vs=&vt;
			packs[i].vd=this;
			packs[i].n=i;
		}
		for(int i=0;i<tablesize.size();i++){
			pthread_create(threads+i,NULL,copythread,packs+i);
		}
		for(int i=0;i<tablesize.size();i++){
			pthread_join(threads[i],NULL);
		}
	}else{
		for(int i=0;i<tablesize.size();i++){
			memcpy(values[i],vt.values[i],tablesize[i]*sizeof(prc));
		}
	}
}

std::string VTable::weightPath(int id){
	std::stringstream ss;
	ss<<"weights/"<<"weight";
	for(int i=0;i<tuples.size();i++){
		ss<<std::dec<<tuples[i].size();
		for(int j=0;j<tuples[i].size();j++){
			ss<<std::hex<<tuples[i][j];
		}
	}
	ss<<"_"<<std::dec<<id;
	return ss.str();
}

std::string VTable::weightPathMini(int n,int id){
	std::stringstream ss;
	ss<<"weights/"<<"weight";
	ss<<std::dec<<tuples[n].size();
	for(int j=0;j<tuples[n].size();j++){
		ss<<std::hex<<tuples[n][j];
	}
	ss<<"_"<<std::dec<<id;
	return ss.str();
}

void *storethread(void *args){
	package pack=*(package *)args;
	std::string path=pack.vd->weightPathMini(pack.n,pack.id);
	std::ofstream fout(path, std::ios::out | std::ios::binary);
	fout.write((char*)pack.vd->values[pack.n],pack.vd->tablesize[pack.n]*sizeof(prc));
	fout.close();
	pthread_exit(0);
}

void VTable::store(int id){
	if(CPTH){
		for(int i=0;i<tablesize.size();i++){
			packs[i].vd=this;
			packs[i].n=i;
			packs[i].id=id;
		}
		for(int i=0;i<tablesize.size();i++){
			pthread_create(threads+i,NULL,storethread,packs+i);
		}
		for(int i=0;i<tablesize.size();i++){
			pthread_join(threads[i],NULL);
		}
	}else{
		std::string path=weightPath(id);
		
		std::ofstream fout(path, std::ios::out | std::ios::binary);
		for(int i=0;i<tablesize.size();i++){
			fout.write((char*)values[i],tablesize[i]*sizeof(prc));
		}
		fout.close();
	}
		
	std::cout<<"Weight saved: "<<id<<'\n';
}

void *restorethread(void *args){
	package pack=*(package *)args;
	std::string path=pack.vd->weightPathMini(pack.n,pack.id);
	std::ifstream fin(path, std::ios::in | std::ios::binary);
	fin.read((char*)pack.vd->values[pack.n],pack.vd->tablesize[pack.n]*sizeof(prc));
	fin.close();
	pthread_exit(0);
}

void VTable::restore(int id){
	if(CPTH){
		for(int i=0;i<tablesize.size();i++){
			packs[i].vd=this;
			packs[i].n=i;
			packs[i].id=id;
		}
		for(int i=0;i<tablesize.size();i++){
			pthread_create(threads+i,NULL,restorethread,packs+i);
		}
		for(int i=0;i<tablesize.size();i++){
			pthread_join(threads[i],NULL);
		}
	}else{
		std::string path=weightPath(id);
		
		try{
			std::ifstream fin(path, std::ios::in | std::ios::binary);
			for(int i=0;i<tablesize.size();i++){
				fin.read((char*)values[i],tablesize[i]*sizeof(prc));
			}
			fin.close();
		}catch(std::exception e){
			std::cout<<"Weight file not found, the weight will be initialized.\n";
			init();
			return;
		}
	}
	std::cout<<"Weight loaded: "<<id<<'\n';
}

void VTable::del(int id){
	if(CPTH){
		for(int i=0;i<tablesize.size();i++){
			std::string path=weightPathMini(i,id);
			std::string inst="rm "+path;
			system(inst.c_str());
		}
	}else{
		std::string path=weightPath(id);
		std::string inst="rm "+path;
		system(inst.c_str());
	}
}

void VTable::init(){
	for(int i=0;i<tablesize.size();i++){
		memset(values[i], 0, tablesize[i]*sizeof(prc));
	}
}
