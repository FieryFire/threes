#include "randomBot.h"
#include "../../globalHeaders.h"
#include <cstdlib>
#include <ctime>
#include <string>
#include <chrono>

RandomBot::RandomBot():PEBot(){
	srand(time(NULL));
	PEBot::name="Random";
}

RandomBot::RandomBot(PEGame &game):PEBot(game){
	srand(time(NULL));
	PEBot::name="Random";
}

RandomBot::RandomBot(PEGame &game,std::string *buff,std::chrono::milliseconds *ms):PEBot(game,buff,ms){
	srand(time(NULL));
	PEBot::name="Random";
}

void RandomBot::play(PEGame &game){
	int reward,act;
	do{
		act=rand()%PEBot::actions;
		reward=game.play(act);
	}while(reward==NOMOVE);
	
	if(reward!=INVALID){
		PEBot::record(act,reward);
	}
	
	game.genTile(act);
}