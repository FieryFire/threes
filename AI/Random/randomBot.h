#ifndef _RANDOM_BOT_H_
#define _RANDOM_BOT_H_

#include "../../globalHeaders.h"
#include <string>
#include <chrono>

class RandomBot: public PEBot{
	protected:
		
	public:
		RandomBot();
		RandomBot(PEGame &);
		RandomBot(PEGame &,std::string *,std::chrono::milliseconds *);
		void play(PEGame &);
};

#endif