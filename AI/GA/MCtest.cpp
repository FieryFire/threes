#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include "../../Game/player_evil_game.h"
#include "../../Game/threes.h"
#include "MCbot.cpp"

using namespace std;

int main(){
	srand(time(NULL));
	Threes game=Threes(9,0,0);
	float gene[240];
	for(int i=0;i<240;i++){
		if(i<16) gene[i]=1;
		else gene[i]=0;
	}
	MCBot bot=MCBot(game,gene);
	int simulations=10;
	time_t st,et;
	time(&st);
	for(int imp=0;imp<simulations;imp++){
		int steps=0;
		game.init();
		while(!game.checkDead()){
			bot.play(game);
			++steps;
		}
		int score=game.getScore();
		cout<<"Simulation "<<imp+1<<": "<<log(score)/log(3)+2<<' '<<steps<<"steps\n";
	}
	time(&et);
	cout<<"Elapsed time: "<<et-st<<" seconds.\n";
	return 0;
}