#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include "../../Game/player_evil_game.h"
#include "../../Game/threes.h"
#include "MCbot.cpp"

using namespace std;

int population=20;
int survive=6;
int cross_size=10;
int cross_pairs[10][2]={0,1,0,2,0,3,0,4,0,5,1,2,1,3,1,4,2,3,2,4};
float mutation_rate=0.1;
int born=4;
int genome_size=1<<17;
float **bot_genes;
float *bot_scores;

int simu_time=10;
int ga_round=1000;
int promp_cycle=10;
int store_cycle=100;

void init();
void simulation(PEGame &);
void selection();
void crossover();
void new_born();
void clear();

int main(){
	Threes game=Threes(9,0,0);
	time_t st,et;
	time(&st);
	init();
	for(int n=0;n<ga_round;n++){
		simulation(game);
		if((n+1)%promp_cycle==0){
			cout<<"Simulation round "<<n+1<<" completed.\n";
		}
		if((n+1)%store_cycle==0){
			fstream result("result.txt",ios::app);
			result<<"Round "<<n+1<<":\n";
			for(int i=0;i<population;i++){
				result<<"Bot "<<i+1<<": "<<bot_scores[i]<<'\n';
			}
			result<<'\n';
			result.close();
			stringstream ss;
			ss<<"weights/weight_"<<n+1;
			string fname=ss.str();
			fstream weight(fname.c_str(),ios::out|ios::binary);
			for(int i=0;i<population;i++){
				weight.write((char *)bot_genes[i],genome_size*sizeof(int));
			}
			weight.close();
		}
		if(n==ga_round-1) break;
		selection();
		crossover();
		new_born();
	}
	time(&et);
	cout<<"Elapsed time: "<<et-st<<" seconds.\n";
	return 0;
}

float random_gene(){
	return ((float)rand()/(float)RAND_MAX)*2-1.0;
}

void init(){
	bot_genes=(float **)malloc(population*sizeof(float *));
	for(int i=0;i<population;i++){
		bot_genes[i]=(float *)malloc(genome_size*sizeof(float));
		for(int j=0;j<genome_size;j++){
			bot_genes[i][j]=random_gene();
		}
	}
	bot_scores=(float *)malloc(population*sizeof(float));
}

void simulation(PEGame &game){
	for(int i=0;i<population;i++){
		MCBot bot=MCBot(game,bot_genes[i]);
		bot_scores[i]=0.0;
		for(int j=0;j<simu_time;j++){
			game.init();
			while(!game.checkDead()){
				bot.play(game);
			}
			int score=game.getScore();
			bot_scores[i]+=(log(score)/log(3)+2.0)/simu_time;
		}
	}
}

void selection(){
	for(int i=0;i<survive;i++){
		for(int j=population-1;j>=i;j--){
			if(bot_scores[j]<bot_scores[j+1]){
				float tmp=bot_scores[j];
				bot_scores[j]=bot_scores[j+1];
				bot_scores[j+1]=tmp;
				float *tmp2=bot_genes[j];
				bot_genes[j]=bot_genes[j+1];
				bot_genes[j+1]=tmp2;
			}
		}
	}
}

void crossover(){
	for(int i=0;i<cross_size;i++){
		for(int j=0;j<genome_size;j++){
			if((float)rand()/RAND_MAX>mutation_rate){
				int select=rand()%2;
				float error=random_gene()/100;
				bot_genes[i+survive][j]=bot_genes[cross_pairs[i][select]][j]+error;
				if(bot_genes[i+survive][j]>1)
					bot_genes[i+survive][j]=1;
				if(bot_genes[i+survive][j]<-1)
					bot_genes[i+survive][j]=-1;
			}else{
				bot_genes[i+survive][j]=random_gene();
			}
		}
	}
}

void new_born(){
	for(int i=0;i<born;i++){
		for(int j=0;j<genome_size;j++){
			bot_genes[i+survive+cross_size][j]=random_gene();
		}
	}
}

void clear(){
	for(int i=0;i<population;i++){
		free(bot_genes[i]);
	}
	free(bot_genes);
	free(bot_scores);
}