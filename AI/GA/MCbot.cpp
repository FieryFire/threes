#ifndef _MCBOT_CPP_
#define _MCBOT_CPP_

typedef float prc;

#include "../../Game/player_evil_game.h"

#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>

class MCBot{
	protected:
		int actions;
		int features;
		std::string name;
		prc *genes;
		prc value(PEGame &game){
			//1-tuple
			if(game.checkDead())
				return -1;
			prc tmp=0.0;
			std::vector<int> s=game.getState();
			for(int i=0;i<features;i++){
				//dot product
				//tmp+=genes[i]*s[i]/features;
				//1-tuple network
				tmp+=genes[s[i]*features+i]/features;
			}
			return tmp;
			
			/*//4-tuple 2x8
			if(game.checkDead())
				return -1;
			prc tmp=0.0;
			for(int i=0;i<2;i++){
				for(int j=0;j<4;j++){
					std::vector<int> s=game.getState();
					int id=s[0]+(s[1]<<4)+(s[2]<<8)+(s[3]<<12);
					tmp+=genes[id]/16;
					id=s[4]+(s[5]<<4)+(s[6]<<8)+(s[7]<<12)+(1<<16);
					tmp+=genes[id]/16;
					game.rotate();
				}
				game.flip();
			}
			return tmp;
			*/
		}
		
		int simulation(PEGame &game,int depth,int times){
			int simulate=times/actions;
			int act,act2;
			prc *val=(prc *)malloc(actions*sizeof(prc));
			prc *val2=(prc *)malloc(actions*sizeof(prc));
			game.store();
			for(int i=0;i<actions;i++){
				for(int j=0;j<simulate;j++){
					int r=game.play(i);
					if(r==NOMOVE){
						val[i]=NOMOVE;
						val2[i]=NOMOVE;
						break;
					}
					game.genTile(i);
					for(int k=0;k<depth-1;k++){
						if(game.checkDead()){
							break;
						}
						r=NOMOVE;
						while(r==NOMOVE){
							act=rand()%actions;
							r=game.play(act);
						}
						game.genTile(act);
					}
					val[i]+=value(game)/simulate;
					val2[i]+=(prc)game.getScore()/simulate;
					game.restore();
				}
			}
			prc tmpmax=NOMOVE+1;
			act=0;
			for(int i=0;i<actions;i++){
				if(val[i]>tmpmax){
					act=i;
					tmpmax=val[i];
				}
			}
			tmpmax=NOMOVE+1;
			act2=0;
			for(int i=0;i<actions;i++){
				if(val2[i]>tmpmax){
					act2=i;
					tmpmax=val2[i];
				}
			}
			//act=(rand()%10)?act:act2;
			free(val);
			free(val2);
			return act;
		}
	
	public:
		MCBot(PEGame &game,prc *seed){
			actions=game.getActionN();
			features=game.getStateN();
			name="Monte Carlo";
			
			genes=seed;
		}
		
		void play(PEGame &game){
			int act,cnt=0;
			std::vector<int> s=game.getState();
			for(std::vector<int>::iterator it=s.begin();it!=s.end();it++){
				if((*it)==0)
					++cnt;
			}
			act=simulation(game,3+std::min(std::max(10-cnt,0),5),4000);
			game.play(act);
			game.genTile(act);
		}
};

#endif