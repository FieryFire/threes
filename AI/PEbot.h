#ifndef _PEBOT_H_
#define _PEBOT_H_

#include "../Game/player_evil_game.h"
#include <string>
#include <chrono>

class PEBot{
	protected:
		int actions;
		int features;
		std::string name;
		
		std::string *dump;
		std::chrono::milliseconds *gameclock;
		
		void record(int,int);
	public:
		PEBot();
		PEBot(PEGame &);
		PEBot(PEGame &,std::string *,std::chrono::milliseconds *);
		virtual void play(PEGame &);
		
		std::string getName();
};

#endif