#ifndef _TD0_BOT_H_
#define _TD0_BOT_H_

typedef float prc;
#define CPTH 1
#define DTABLE 1

#include "../../Game/player_evil_game.h"
#include "../PEbot.h"

#include <vector>
#include <string>
#include <chrono>
#include <pthread.h>

int power16(const int);
int amax(const int,const prc *);
int vmax(std::vector<int>);
prc alphaDecay(int);
void *copythread(void *);
void *storethread(void *);
void *restorethread(void *);

class VTable;

typedef struct{
	VTable *vs,*vd;
	int n,id;
} package;

class VTable{
	protected:
		std::vector<std::vector<int> > tuples;
		std::vector<int> tablesize;
		prc **values, alpha;
		package *packs;
		pthread_t *threads;
		int level;
		
		std::vector<int> hashTuple(std::vector<int>);
	public:
		VTable();
		VTable(std::vector<std::vector<int> > &, int);
		void destruct();
		
		prc getValue(std::vector<int>);
		void update(std::vector<int>,prc,int);
		void copy(VTable &);
		
		std::string weightPath(int);
		std::string weightPathMini(int,int);
		void store(int);
		void restore(int);
		void del(int);
		void init();
		friend void *copythread(void *);
		friend void *storethread(void *);
		friend void *restorethread(void *);
};

class TD0Bot: public PEBot{
	protected:
		std::vector<std::vector<int> > tuples;
		int tablecnt, blocklevel[10], fixsmall;
		std::vector<VTable> eval_t,target_t;
		double fast_up,search_path;
		double e_greedy,gamma;
		int round,trained;
		int cstep,step_max,maxtile;
		int search_max,train_max;
		int updateCycle,storeCycle;
		std::vector<int> wrecord;
		int record_max;
		
		void init();
		int maxTile(std::vector<int>);
		prc reward(std::vector<int>,std::vector<int>);
		prc penalty(int);
		int calcTable(std::vector<int>);
		int hashState();
	public:
		TD0Bot();
		TD0Bot(PEGame &);
		TD0Bot(PEGame &,std::string *,std::chrono::milliseconds *);
		void destruct();
		void play(PEGame &);
};

#endif
