#include "td0Bot.h"

#include <cstdlib>
#include <ctime>
#include <cmath>
#include <string>
#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <cstring>
#include <chrono>
//#include <pthread.h>

int amax(const int n,const prc *v){
	prc m=-9999999;
	for(int i=0;i<n;i++){
		m=m<v[i]?v[i]:m;
	}
	for(int i=0;i<n;i++){
		if(v[i]==m) return i;
	}
}

int vmax(std::vector<int> v){
	if(!v.size()) return 0;
	int tmp=v[0];
	for(int i=0;i<v.size();i++){
		tmp=tmp<v[i]?v[i]:tmp;
	}
	return tmp;
}

TD0Bot::TD0Bot():PEBot(){
	PEBot::name="TD0";
}

TD0Bot::TD0Bot(PEGame &game):PEBot(game){
	PEBot::name="TD0";
	init();
}

TD0Bot::TD0Bot(PEGame &game,std::string *buff,std::chrono::milliseconds *ms):PEBot(game,buff,ms){
	PEBot::name="TD0";
	init();
}

void TD0Bot::destruct(){
	for(int i=0;i<tablecnt;i++){
		eval_t[i].destruct();
		target_t[i].destruct();
	}
}

void TD0Bot::init(){
	//v1 6*4 (8 rotation)
	int tlength=4;
	int tsize[4]={6,6,6,6};
	int tblocks[4][6]={{0,1,2,3,4,5},{4,5,6,7,8,9},{5,6,7,9,10,11},{9,10,11,13,14,15}};

	for(int i=0;i<tlength;i++){
		tuples.push_back(std::vector<int>(tblocks[i],tblocks[i]+tsize[i]));
	}
	//changable variables:
	//v1
	trained=0;
	round=0;
	search_max=5000000;
	train_max=5000000;
	updateCycle=10;
	storeCycle=1000000;
	record_max=10;
	
	fast_up=0.95;
	search_path=0.8;
	/*
	fast_up=1.0;
	search_path=0.95;
	*/
	gamma=0.9;
	tablecnt=2;
	blocklevel[0]=9;
	blocklevel[1]=11;
	fixsmall=1;
	for(int i=0;i<tablecnt;i++){
		eval_t.push_back(VTable(tuples,i));
		//eval_t[i]=VTable(tuples,i);
		if(trained){
			eval_t[i].restore(trained);
		}else{
			eval_t[i].init();
		}
		if(DTABLE&&round<train_max){
			target_t.push_back(VTable(tuples,i));
			//target_t[i]=VTable(tuples,i);
			target_t[i].copy(eval_t[i]);
		}
	}
	
	cstep=9;
	step_max=0;
	maxtile=0;
	srand(time(NULL));
}

int TD0Bot::maxTile(std::vector<int> s){
	int i=0;
	for(int imp=0;imp<16;imp++){
		i=i<s[imp]?s[imp]:i;
	}
	return i;
}

prc TD0Bot::reward(std::vector<int> S0,std::vector<int> S1){
	int b0[16]={0},b1[16]={0},tmpscore=0;
	for(int i=0;i<16;i++){
		b0[S0[i]]++;
		b1[S1[i]]++;
	}
	for(int i=15;i>2;i--){
		b1[i]-=b0[i];
		if(b1[i]>0){
			if(i>3)
				tmpscore+=1<<(i-3);
			else
				tmpscore+=10*b1[i];
			b0[i-1]-=2*b1[i];
		}
	}
	if(tmpscore==0) tmpscore=1;
	return tmpscore;
}

prc TD0Bot::penalty(int step){
	return -20;
}

int expected_steps(int tile){
	if(tile<4) return 1;
	else return 3<<(tile-4);
}

int TD0Bot::calcTable(std::vector<int> s){
	int bmax=vmax(s);
	for(int tmp=0;tmp<tablecnt-1;tmp++){
		if(bmax<blocklevel[tmp])
			return tmp;
	}
	return tablecnt-1;
}

void TD0Bot::play(PEGame &game){
	int reward,act,boardmax,crttable;
	std::vector<int> s0[8],a0,s1[8];
	for(int flip=0;flip<2;flip++){
		for(int rotate=0;rotate<4;rotate++){
			s0[flip*4+rotate]=game.getState();
			game.rotate();
		}
		game.flip();
	}
	crttable=calcTable(s0[0]);
	
	boardmax=vmax(s0[0]);
	if(round>=train_max||cstep<expected_steps(maxtile-2))
		e_greedy=2.0; //1 is fine :)
	else if(cstep<expected_steps(maxtile-1))
		e_greedy=fast_up;
	else
		e_greedy=search_path;
	//action strategy
	if((double)rand()/(double)RAND_MAX<e_greedy){
		//peek the next state and find the best value one
		game.store();
		prc asv[4]={0.0};
		for(int i=0;i<4;i++){
			reward=game.play(i);
			if(reward==NOMOVE){
				asv[i]=reward;
				continue;
			}
			game.genTile(i,0);
			for(int flip=0;flip<2;flip++){
				for(int rotate=0;rotate<4;rotate++){
					prc tmp;
					s1[0]=game.getState();
					tmp=eval_t[crttable].getValue(s1[0]);
					//asv[i]=tmp>asv[i]?tmp:asv[i];
					asv[i]+=tmp/8;
					game.rotate();
				}
				game.flip();
			}
			game.restore();
		}
		act=amax(4,asv);
		reward=game.play(act);
	}else{
		//randomly action for a small probability to reach more diversity
		do{
			act=rand()%4;
			reward=game.play(act);
		}while(reward==NOMOVE);
	}
	a0=game.getState();
	
	if(reward!=INVALID){
		PEBot::record(act,reward);
	}
	
	game.genTile(act);
	game.store();
	cstep++;
	
	if(crttable>=fixsmall&&round<train_max){
		prc asv[4]={0.0,0.0,0.0,0.0};
		int mnext;
		prc maxval=0;
		prc r2;
		for(int flip=0;flip<2;flip++){
			for(int rotate=0;rotate<4;rotate++){
				s1[flip*4+rotate]=game.getState();
				game.rotate();
			}
			game.flip();
		}
		if(DTABLE){
			for(int i=0;i<8;i++){
				prc tmp;
				tmp=target_t[crttable].getValue(s1[i]);
				//maxval=tmp>maxval?tmp:maxval;
				maxval+=tmp/8;
			}
			prc r2=reward+gamma*maxval;
			for(int t=fixsmall;t<=crttable;t++){
				for(int i=0;i<8;i++){
					target_t[t].update(s0[i],r2,round);
				}
			}
			if(game.checkDead()){
				for(int t=fixsmall;t<=crttable;t++){
					for(int i=0;i<8;i++){
						target_t[t].update(s1[i],penalty(cstep),round);
					}
				}
			}
		}else{//Not currently used
			for(int i=0;i<8;i++){
				prc tmp;
				tmp=eval_t[crttable].getValue(s1[i]);
				//maxval=tmp>maxval?tmp:maxval;
				maxval+=tmp/8;
			}
			prc r2=TD0Bot::reward(s0[0],a0)+gamma*maxval;
			for(int t=fixsmall;t<=crttable;t++){
				for(int i=0;i<8;i++){
					eval_t[crttable].update(s0[i],r2,round);
				}
			}
			if(game.checkDead()){
				for(int t=fixsmall;t<=crttable;t++){
					for(int i=0;i<8;i++){
						eval_t[crttable].update(s1[i],penalty(cstep),round);
					}
				}
			}
		}
	}
	if(game.checkDead()){
		step_max=cstep>step_max?cstep:step_max;
		cstep=0;
		boardmax=vmax(game.getState());
		maxtile=maxtile<boardmax?boardmax:maxtile;
		round++;
		if(DTABLE&&round<=train_max&&round%updateCycle==0){
			for(int i=fixsmall;i<tablecnt;i++){
				eval_t[i].copy(target_t[i]);
			}
		}
		if(fast_up<1) //reserved for dynamically adjusted searching criteria
			;//fast_up=search_path=fast_up+0.1/search_max;
		/*
		if(round==search_max){
			fast_up=1.0;
			search_path=0.9;
		}
		*/
		if(DTABLE&&round<=train_max&&round%storeCycle==0){
			for(int i=0;i<tablecnt;i++)
				target_t[i].store(round);
			if(wrecord.size()==record_max){
				for(int i=0;i<tablecnt;i++)
					eval_t[i].del(wrecord[0]);
				wrecord.erase(wrecord.begin());
			}
			wrecord.push_back(round);
		}
		if(round<=train_max&&round%10000==0){
			std::cout<<"Round: "<<round<<'\n';
		}
	}
}
