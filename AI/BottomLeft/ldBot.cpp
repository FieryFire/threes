#include "ldBot.h"
#include <string>
#include <chrono>

LDBot::LDBot():PEBot(){
	PEBot::name="LDCorner";
	lastAction=1;
}

LDBot::LDBot(PEGame &game):PEBot(game){
	PEBot::name="LDCorner";
	lastAction=1;
}

LDBot::LDBot(PEGame &game,std::string *buff,std::chrono::milliseconds *ms):PEBot(game,buff,ms){
	PEBot::name="LDCorner";
	lastAction=1;
}

void LDBot::play(PEGame &game){
	int reward,act;
	reward=game.play(3-lastAction);
	act=3-lastAction;
	if(reward==NOMOVE){
		reward=game.play(lastAction);
		act=lastAction;
		if(reward==NOMOVE){
			reward=game.play(3);
			act=3;
			if(reward==NOMOVE){
				reward=game.play(0);
				act=0;
				lastAction=2;
			}else{
				lastAction=1;
			}
		}
	}else{
		lastAction=3-lastAction;
	}
	
	if(reward!=INVALID){
		PEBot::record(act,reward);
	}
	
	game.genTile(act);
}