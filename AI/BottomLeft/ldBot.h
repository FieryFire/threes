#ifndef _LDBOT_H_
#define _LDBOT_H_

#include "../../Game/player_evil_game.h"
#include "../PEbot.h"
#include <string>
#include <chrono>

class LDBot: public PEBot{
	protected:
		int lastAction;
	public:
		LDBot();
		LDBot(PEGame &);
		LDBot(PEGame &,std::string *,std::chrono::milliseconds *);
		void play(PEGame &);
};

#endif