#ifndef GHEADER_H
#define GHEADER_H

#define LDCORNER

#include "Game/player_evil_game.h"
#include "Game/threes.h"
#include "AI/PEbot.h"

#ifdef RANDOM
#include "AI/Random/randomBot.h"
#define UsedBot RandomBot
#endif

#ifdef LDCORNER
#include "AI/BottomLeft/ldBot.h"
#define UsedBot LDBot
#endif

#ifdef TD0
#include "AI/TD0/td0Bot.h"
#define UsedBot TD0Bot
#endif

#endif